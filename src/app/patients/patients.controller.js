(function() {
  'use strict';

  angular
    .module('aps')
    .controller('PatientsController', PatientsController);

  /** @ngInject */
  function PatientsController($log, $scope, $state, data, mhs, moment) {
    var vm = this;
    activate();

    function activate() {
      $log.log('data', data);
      vm.patients = patientList(data.patients);
      $log.log('starting Patients.');
      mhs.loggedInOrDeny();
    }

    // convert a rawlist as supplied by server into displayable list:
    function patientList(patients) {
      var list = [];
      _.forEach(patients, function(patientIn) {
        var patient = _.extend({
          location: '?',
          aps_status: 'current'
        }, patientIn);
        patient.age = moment().diff(patient.dob, 'years');
        delete patient.dob;
        list.push(patient);
      });
      return list;
    }

    // watch the selected item and go to scope if needed
    $scope.$watchCollection("patients.selected", function(){
      if (vm.selected && vm.selected[0]) {
        $log.log('patient' , vm.selected[0]);
        $state.go('patient', {urn: vm.selected[0].id});
      }
    });

  }
})();
