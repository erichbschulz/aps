(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('patients', {
        url: '/patients',
        templateUrl: 'app/patients/patients.html',
        controller: 'PatientsController',
        controllerAs: 'patients',
        resolve: {
          data: ['$log', 'apsService', function($log, apsService) {
            return apsService.getPatients()
           .then(
              function(patients){
                $log.log('patients loaded');
                return {error: false, patients: patients}
              },
              function(error){
                $log.log('failed patient load');
                return {error:error}
              }
            );
          }]
        }
      });


  }

})();
