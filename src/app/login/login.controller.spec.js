(function() {
  'use strict';

  describe('controllers', function(){
    var vm;
    var toastr;

    beforeEach(module('aps'));
    beforeEach(inject(function(_$controller_, _$timeout_, _webDevTec_, _toastr_) {
      spyOn(_webDevTec_, 'getTec').and.returnValue([{}, {}, {}, {}, {}]);
      spyOn(_toastr_, 'info').and.callThrough();
      vm = _$controller_('MainController');
      toastr = _toastr_;
    }));

    it('should have a timestamp creation date', function() {
      expect(vm.creationDate).toEqual(jasmine.any(Number));
    });

    it('should show a Toastr info', function() {
      vm.showToastr();
      expect(toastr.info).toHaveBeenCalled();
    });

  });
})();
