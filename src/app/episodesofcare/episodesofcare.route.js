(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('EpisodesOfCare', {
        url: '/EpisodeOfCare',
        templateUrl: 'app/episodesofcare/episodesofcare.html',
        controller: 'EpisodesOfCareController',
        controllerAs: 'vm',
        resolve: {
          data: ['$log', 'apsService', function($log, apsService) {
            return apsService.getEpisodesOfCare()
           .then(
              function(EpisodesOfCare){
                $log.log('EpisodesOfCare loaded');
                return {error: false, EpisodesOfCare: EpisodesOfCare}
              },
              function(error){
                $log.log('failed patient load');
                return {error:error}
              }
            );
          }]
        }
      });


  }

})();
