'use strict';
(function() {

  angular
    .module('aps')
    .controller('EpisodesOfCareController', EpisodesOfCareController);

  /** @ngInject */
  function EpisodesOfCareController($log, $scope, $state, data, mhs) {
    var vm = this;
    activate();

    function activate() {
      vm.EpisodesOfCare = EpisodeOfCareList(data.EpisodesOfCare);
      mhs.loggedInOrDeny();
    }

    // convert a rawlist as supplied by server into displayable list:
    function EpisodeOfCareList(EpisodesOfCare) {
      var list = [];
      _.forEach(EpisodesOfCare, function(EpisodeOfCareIn) {
        var EpisodeOfCare = _.extend({
          location: '?',
          aps_status: 'current'
        }, EpisodeOfCareIn);
//        EpisodeOfCare.age = moment().diff(EpisodeOfCare.dob, 'years');
//        delete EpisodeOfCare.dob;
        list.push(EpisodeOfCare);
      });
      return list;
    }

    // watch the selected item and go to scope if needed
    $scope.$watchCollection("vm.selected", function(){
      // find the first element in the array if it exists
      var selected = (vm.selected || [])[0] ;
      if (selected) {
        $state.go('patient', {urn: selected.patient_id});
      }
    });

  }
})();
