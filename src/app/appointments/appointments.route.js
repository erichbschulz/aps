(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('Appointments', {
        url: '/Appointments',
        templateUrl: 'app/appointments/appointments.html',
        controller: 'AppointmentsController',
        controllerAs: 'vm',
        resolve: {
          data: ['$log', 'apsService', function($log, apsService) {
            return apsService.getAppointments()
           .then(
              function(Appointments){
                $log.log('Appointments loaded');
                return {error: false, Appointments: Appointments}
              },
              function(error){
                $log.log('failed EpisodeOfCare load');
                return {error:error}
              }
            );
          }]
        }
      });


  }

})();
