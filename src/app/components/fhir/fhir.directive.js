(function() {
  'use strict';

  angular
    .module('aps')
    .directive('mmlFhir', mmlFhir) // collection handler
    .directive('mmlFhirJson', mmlFhirJson) // expandable json
    .directive('mmlPatientFlag', mmlPatientFlag) // for Flag resource
    .directive('mmlMedicationStatement', mmlMedicationStatement);

  // directive to handle polymorphic collections
  /** @ngInject */
  function mmlFhir($log) {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/fhir/fhir.html',
      scope: {
        resource: '@', //resource type
        data: '='
      },
      controller: fhirController,
      controllerAs: 'vm',
      bindToController: true
    };
    return directive;

    /** @ngInject */
    function fhirController($scope) {
      $log.log('$scope', $scope);
    }
  }

  /** @ngInject */
  function mmlPatientFlag() {
    var directive = {
      restrict: 'E',
      scope: {
        item: '=',
        expanded: '='
      },
      templateUrl: 'app/components/fhir/flag.html',
      controller: controller,
      controllerAs: 'vfm',
      bindToController: true
    };
    return directive;
    /** @ngInject */
    function controller() {
      var vfm = this;
      vfm.item2 = transformForView(vfm.item);
    }
  }

  /** @ngInject */
  function mmlMedicationStatement($log) {
    var directive = {
      restrict: 'E',
      scope: {
        item: '=',
        expanded: '='
      },
      templateUrl: 'app/components/fhir/medicationStatement.html',
      controller: controller,
      controllerAs: 'vfm',
      bindToController: true
    };
    return directive;
    /** @ngInject */
    function controller($scope) {
      $log.log('MedicationStatement scope', $scope);
      var vfm = this;
      vfm.item2 = transformForView(vfm.item);
    }
  }

  /**
    * perform standard transformations
    */
  function transformForView(item) {
    var item2 = _.omit(item, function(item) {
      return _.isEmpty(item);
    });
    return item2;
  }

  /** @ngInject */
  function mmlFhirJson() {
    var directive = {
      restrict: 'E',
      scope: {
        item: '='
      },
      templateUrl: 'app/components/fhir/mmlJson.html',
      controller: controller,
      controllerAs: 'vjm',
      bindToController: true
    };
    return directive;
    /** @ngInject */
    function controller() {
      var vjm = this;
      this.toggle = function(event) {
        vjm.jsonOpen = !vjm.jsonOpen;
        event.stopPropagation();
      };
    }
  }

})();
