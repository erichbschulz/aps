(function() {
  'use strict';

  angular
    .module('aps')
    .controller('AppointmentsController', AppointmentsController);

  /** @ngInject */
  function AppointmentsController($log, $scope, $state, data, mhs) {
    var vm = this;
    activate();

    function activate() {
      vm.Appointments = appointmentList(data.Appointments);
      mhs.loggedInOrDeny();
    }

    // convert a rawlist as supplied by server into displayable list:
    function appointmentList(Appointments) {
      var list = [];
      _.forEach(Appointments, function(appointmentIn) {
        var appointment = _.extend({
          extra: '?'
        }, appointmentIn);
//        appointment.age = moment().diff(appointment.dob, 'years');
//        delete appointment.dob;
        list.push(appointment);
      });
      return list;
    }

    // watch the selected item and go to scope if needed
    $scope.$watchCollection("vm.selected", function(){
      // find the first element in the array if it exists
      var selected = (vm.selected || [])[0] ;
      if (selected) {
        $state.go('patient', {urn: selected.patient_id});
      }
    });

  }
})();
