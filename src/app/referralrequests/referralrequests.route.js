(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('ReferralRequests', {
        url: '/ReferralRequests',
        templateUrl: 'app/referralrequests/referralrequests.html',
        controller: 'AppointmentsController',
        controllerAs: 'vm',
        resolve: {
          data: ['$log', 'apsService', function($log, apsService) {
            return apsService.getAppointments()
           .then(
              function(ReferralRequests){
                $log.log('ReferralRequests loaded');
                return {error: false, ReferralRequests: ReferralRequests}
              },
              function(error){
                $log.log('failed EpisodeOfCare load');
                return {error:error}
              }
            );
          }]
        }
      });


  }

})();
