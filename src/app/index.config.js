(function() {
  'use strict';

  angular
    .module('aps')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, localStorageServiceProvider
    ,  $httpProvider
    ) {
    // Enable log
    $logProvider.debugEnabled(true);
    // enable credentials
    $httpProvider.defaults.withCredentials = true;

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
    localStorageServiceProvider.setPrefix('mhs_aps')
      .setNotify(true, true);
  }


})();
