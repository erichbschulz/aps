(function() {
  'use strict';

angular
  .module('aps')
  .controller('PatientRegisterController', PatientController);

/** @ngInject */
function PatientController($log, $scope, $state, data, apsService, mhs) {
  $log.log('starting patient controller');
  var vm = this;
  var patient_id;
  activate();

  function activate() {
    mhs.loggedInOrDeny();
    $log.log('data', data);
    vm.rec = data;
    patient_id = vm.rec.patient.id;
    $log.log('patient_id', patient_id);
    vm.schema = apsService.registerSchema(vm.rec);
    vm.form = apsService.registerForm(vm.rec);
  }

  vm.model = {
    pca: 'fentanyl',
    ketamine: true
  };

  vm.onSubmit = function() {
    // broadcast an event so all fields validate themselves
    $scope.$broadcast('schemaFormValidate');
    apsService.saveReferral(patient_id, vm.model).then(
      function(message) {
        $log.log('message', message);
        $state.go('patients');
      },
      function(message) {
        alert('boo' + message)
      })
  }

  vm.cancel = function() {
    $log.log('cancelling');
    $state.go('patients');
  }


}
})();
