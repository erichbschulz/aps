(function() {
  'use strict';

  angular
    .module('aps')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, mhs, webDevTec,
    toastr, localStorageService) {

    var vm = this;


    activate();

    function activate() {
      vm.creationDate = 1445512823560;
      vm.showToastr = showToastr;
      vm.server = mhs.getServerUrl();
      vm.storageType = localStorageService.getStorageType();
      vm.storageSupported = (localStorageService.isSupported
        ? 'ok'
        : 'not supported');
      localStorageService.set('my_id', 1201);
      vm.storageSize = localStorageService.length();
    }

    function showToastr() {
      toastr.info('This system is a protype.');
    }

  }
})();
